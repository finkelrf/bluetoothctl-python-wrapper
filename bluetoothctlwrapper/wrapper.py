import re
import time
import logging
import pexpect as pex

DEBUG = True
LOGGER = True
PRINT = False

default_print = print


def print(msg):
    if DEBUG:
        if LOGGER:
            logging.debug(msg)
        if PRINT:
            default_print(msg)


class Bt:
    def __init__(self):
        self.p = pex.spawn('bluetoothctl', encoding='utf-8')

    def _read_all(self, timeout=5):
        '''
        Read all characters from the buffer unit endpattern or timeout
        '''
        endpattern = b"\x1b[?2004h\x1b[0;94m[bluetooth]\x1b[0m#"
        output = ""
        done = False
        while not done:
            try:
                output += self.p.read_nonblocking(1, timeout)
                if endpattern in output.encode('utf-8'):
                    done = True
            except pex.exceptions.TIMEOUT as e:
                done = True
        return output

    def remove_device(self, device_name):
        print(f'Removing {device_name} if already paired')

        self.p.sendline('devices')
        done = False
        response = ''
        while not done:
            try:
                letter = self.p.read_nonblocking(1, 1)
                response += letter
            except pex.exceptions.TIMEOUT as e:
                done = True

        devices = re.findall(
            r"Device (\w\w\:\w\w\:\w\w\:\w\w\:\w\w\:\w\w) ([\w -]*)", response)
        for device in devices:
            address, name = device
            if "Wireless Controller" == name:
                print(f'Removing {device_name}')
                self.p.sendline(f'remove {address}')

    def scan_on(self, scan_time=1):
        self.empty_buffer()
        self.p.sendline('scan on')
        time.sleep(scan_time)

    def scan_off(self,):
        self.empty_buffer()
        self.p.sendline('scan off')

    def get_devices(self):
        devices = {}
        DEVICE_PATTERN = r"Device (?P<device_mac>\w\w:\w\w:\w\w:\w\w:\w\w:\w\w)\s(?P<device_name>.*)"
        device_regex = re.compile(DEVICE_PATTERN)
        self.empty_buffer()
        self.p.sendline('devices')
        output = self._read_all()

        matches = device_regex.finditer(output)
        for match in matches:
            devices[match.group('device_name').replace(
                "\r", "")] = match.group('device_mac')

        return devices

    def scan_for_device(self, device_name):
        self.empty_buffer()
        print('Starting scan')
        self.p.sendline('scan on')

        done = False
        line = ''
        dev_addr = []
        while not done:
            try:
                letter = self.p.read_nonblocking(1, 10)
                line += letter
                if letter == '\n':
                    print(f'--------------line: {line}')
                    if 'Wireless Controller' in line:
                        dev_addr = re.search(
                            r"\w\w\:\w\w\:\w\w\:\w\w\:\w\w\:\w\w", line).group()
                        return dev_addr
                    line = ''

            except pex.exceptions.TIMEOUT as e:
                done = True
        return ''

    def connect(self, device_address, timeout=-1):
        self.empty_buffer()
        print('scan off')
        self.p.sendline('scan off')
        self.p.expect('#')
        print(f'connect {device_address}')
        self.p.sendline(f'connect {device_address}')
        try:
            return self.p.expect(["Failed to connect", "Connection successful"], timeout=timeout)
        except pex.exceptions.TIMEOUT:
            raise TimeoutError

    def exit(self):
        print('Exit')
        self.p.sendline('exit')
        self.p.expect('#')

    def trust(self, device_address):
        self.p.sendline(f'trust {device_address}')
        self.p.expect('#')

    def empty_buffer(self, debug=False):
        buffer_data = ''
        if debug:
            print('=========================================================')
        try:
            while True:
                buffer_data += self.p.read_nonblocking(timeout=1)
        except pex.exceptions.TIMEOUT as ex:
            if debug:
                print(buffer_data)

        if debug:
            print('=========================================================')


if __name__ == '__main__':
    DEBUG = True
    LOGGER = False
    PRINT = True
    bt = Bt()
    bt.scan_on(3)
    devices = bt.get_devices()
    print(devices)
    device_name = input("Enter device name: ")
    con = bt.connect(devices[device_name])
    print(con)
