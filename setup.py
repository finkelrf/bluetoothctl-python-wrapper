import setuptools

with open("README.md", "r", encoding="utf-8") as fh:
    long_description = fh.read()

setuptools.setup(
    name="bluetoothctlwrapper", # Replace with your own username
    version="0.0.1",
    author="Rafael Finkelstein",
    author_email="finkel.rf@gmail.com",
    description="A Python Wrapper for Bluetoothctl",
    long_description=long_description,
    long_description_content_type="text/markdown",
    url="https://gitlab.com/finkelrf/bluetoothctl-python-wrapper.git",
    packages=setuptools.find_packages(),
    classifiers=[
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: MIT License",
        "Operating System :: Linux",
    ],
    install_requires=[
        'pexpect',
    ]
)